<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script type="text/javascript" src="./public/js/jquery.min.js"></script>
        <!--<script type="text/javascript" src=".public/js/jquery.min.js"></script>-->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                /*background-color: #fff;*/
                color: #000000;
                font-family: 'Arial', sans-serif;
                /*font-weight: 100;
                height: 100vh;
                margin: 0;*/
            }

            .full-height {
                height: 100vh;
            }
            td{
                color:black;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                /*color: #636b6f;*/
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            input{
                text-transform:uppercase;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <!-- <div>
            <input type="text" class="form-control " style="font-size: 16px;" id="busca_curp"  >
            <button type="button" class="btn btn-default " id="buscar"><i class="icon-user-plus position-center"></i>BUSCAR</button>
        </div> -->
        <!-- style='position:relative;display:none' -->
       <!--  <div class="" id="datos_respuesta" >
         

          <div class="">
            <table id="cortecajavista" class="table">
            
            <tbody id="mostrarcuentasdia">
             
                     
                <tr>
                    <td class="td"><div> Nombre </div></td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="nombre" id="nombre" class=" form-control"  class="form-control" value=""  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Apellido paterno</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="apaterno" id="apaterno" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Apellido materno</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="amaterno" id="amaterno" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Sexo</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="sexo" id="sexo" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
               
                <tr>
                    <td>Curp</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="curp" id="curp" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>RFC</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="rfc" id="rfc" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Estado</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="estado" id="estado" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Municipio</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="municipio" id="municipio" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Ejido</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="localidad" id="localidad" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
                <tr>
                    <td>Partido</td>

                    <td align="center">
                        <div class="col-md-6" >
                            <input type="text" name="partido" id="partido" class=" form-control"  class="form-control"  >        
                        </div>
                    </td>  
                </tr>
            </tbody>
            <tfoot>
            </tfoot>
          </table>
          </div>
        </div> -->
        <br>
        <div>
            <button type="button" class="btn btn-default " id="pan"><i class="icon-user-plus position-center"></i>Mostrar</button>
            <button type="button" class="btn btn-default " id="pri"><i class="icon-user-plus position-center"></i>Agregar</button>            
            <button type="button" class="btn btn-default " id="morena"><i class="icon-user-plus position-center"></i>Actualizar</button>
            <button type="button" class="btn btn-default " id="otros"><i class="icon-user-plus position-center"></i>Borrar</button>
        </div>
        <br>
        <div>
            <button type="button" class="btn btn-default " id="limpiar"><i class="icon-user-plus position-center"></i>Limpiar campos</button>
           
        </div>
        <br>
     
        <input name="token" id="token" type="hidden" value="{{ csrf_token() }}">
    </body>
    <script type="text/javascript" src="./public/js/funciones.js"></script>
    <!--<script type="text/javascript" src="./public/js/funciones.js"></script>-->
</html>

