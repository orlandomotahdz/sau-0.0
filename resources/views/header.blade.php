 <!DOCTYPE html>
<html lang="en">
<head> 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SAU</title>

	@if (Auth::guest())
		 <link href="./css/app.css" rel="stylesheet">
	@else
		<!-- Bootstrap 3.3.5 -->
		<link href="./css/bootstrap.css" rel="stylesheet" >
	    <!-- Font Awesome -->
	     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		
		<!-- Ionicons -->
	    <link rel="stylesheet" href="./css/ionicons.min.css">
	    
		<link href="./css/AdminLTE.min.css" rel="stylesheet" >	
		
		<!-- folder instead of downloading all of them to reduce the load. -->
	    <link rel="stylesheet" href="./css/_all-skins.min.css">

	    <!-- server normal -->
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
	   <!-- BS JavaScript -->
	   <script type="text/javascript" src="./js/bootstrap.min.js"></script>
	 	<!-- <script type="text/javascript" src="./js/libraries/jquery.min.js"></script>  -->
	@endif

	

 
	
</head>
