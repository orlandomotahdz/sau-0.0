@extends('index')

<!-- Content area -->
@section('area')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-10">
				<div class="box">
					<h1>Materia prima</h1>
					<div class="panel-heading">
		            	<p><button type="button" class="btn btn-success btn-lg" id="agregar_materiaprima"><i class="fa fa-fw fa-plus"></i> Agregar</button></p>
					</div>
					<div class="box-body">
						<table class="table" style="padding-left:20px;padding-right:20px;">
							<tr>
								<td>
								 Nombre:
								</td>
								<td>
								 Clave producto:
								</td>
								<td>
								 Precio:
								</td>
							</tr>
							<tbody>
								<?php 
									if(isset($materia)){
										for($i=0; $i<count($materia); $i++ ){ ?>
											<tr data-id="{{$materia[$i]['id']}}">
												<td> {{$materia[$i]['nombre']}} </td>
												<td> {{$materia[$i]['clave_materiaprima']}} </td>
												<td> $ {{$materia[$i]['precio']}}</td>
												
												<td>
													<button type="button" class="btn btn-info verelemento" title="Ver elemento"><i class="fa fa-fw fa-file" ></i></button>						
													<button type="button"  class="btn btn-primary editarelemento btn-icon" title="Editar elemento"><i class="fa fa-fw fa-edit"></i></button>
													<button type="button" data-ruta="delete_inventario"  class="btn btn-danger borrar btn-icon" title="Eliminar elemento"><i class="fa fa-fw fa-remove"></i></button>
												</td>

											</tr>
										<?php 
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@include('materiaprima.agregarmateria')
@include('materiaprima.kiloslitros')
@endsection
