 <div id="kiloslitros_modal" class="modal fade" role="dialog"> 
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header"> 
            <h4 class="modal-tittle">Materia prima por kilos/litros</h4>
          </div> 
          <form class="form-horizontal" role="form" id="form-agregar">
            <div class="modal-body"> 
              <div class="form-group col-md-12">
                <label for="cantidad" class="control-label col-sm-4">Cantidad: </label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="cantidadkiloslitros" name="">
                </div>
              </div> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Cerrar</span>
              </button>
              <button type="button" id="GuardarNombre" name="GuardarNombre" class="btn btn-primary">
                <span class="fa fa-save"></span><span class="hidden-xs"> Guardar</span>
                          
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>