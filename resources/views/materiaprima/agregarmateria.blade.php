<div class="modal fade" id="agregarmateriaprima_modal" role="dialog" >
	<div class="modal-dialog" style="width:50%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"> Agregar</h4>
			</div>


		<div class="modal-body">
			<form  id="agregarmateriaprimaform_modal">

				<div class="panel-body">

					<div class="row ">
						<div class="col-md-6">
							<div class="form-group form-group-material">
								<label class="">Nombre</label>
								<input type="text" id="nombreomateriaprima" name="nombreomateriaprima" class="form-control" placeholder="Nombre materia prima" >
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group form-group-material">
								<label class="">Subtotal</label>
								<input type="text" id="claveomateriaprima" name="claveomateriaprima" class="form-control" placeholder="Clave materia prima" >
							</div>
						</div>
					</div>
					<div class="row ">
						<div class="col-md-4">
							<div class="form-group form-group-material">
								<label class="">Precio</label>
								<input type="text" id="preciomateriaprima" name="preciomateriaprima" class="form-control" placeholder="Precio" >
							</div>
						</div>
						<div class="col-md-8 ">
							<div class="form-group">
								<div>
									<label class="">Tipo de materia prima</label>
								</div>
								<div>
									<div class="col-md-4">
										<button type="button" id="kiloslitros" class="btn btn-block btn-default">Kilo / Litro</button>
									</div>
									<div class="col-md-4">
										<button type="button" id="pieza" class="btn btn-block btn-default">Pieza</button>
									</div>
									<div class="col-md-4">
										<button type="button" id="caja" class="btn btn-block btn-default">Caja</button>
									</div>
								</div>

							</div>


						</div>
					</div>
				</div>

			</form>
		</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button"  class="btn btn-success" id="guardar">Guardar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

