
@include('header')
	@if (Auth::guest())
		<!--LLamamos el login -->
		@yield('content')		
	@else
		<body class="hold-transition skin-blue sidebar-mini">
			<div class="wrapper">
			@include('mainmenu')

			@yield('area')
			<div>
		</body>
	@endif

<input name="token" id="token" type="hidden" value="{{ csrf_token() }}">
@include('footer')