$(document).ready(function(){
	var id =0;
	var validar = false;
	//input que recibe el valor a buscar
	$("#buscar").click(function(){
		console.log($("#busca_curp").val());
		console.log($("#token").val());
		if (!$("#busca_curp").val()) {
			alert("El campo 'BUSCAR' esta vacio")
		}else{
			$.ajax({
				url:"padron/"+$("#busca_curp").val(),
				method:"get",
				dataType: 'json',
				data: {					
					_token : $("#token").val()},
				success:function(respuesta){			
					if(respuesta){
						var r = new Array();
						r.length= 0;
						habilitarcampos();
						//metemos en un arreglo losd atos obtenidos para eliminar el indice 0						
						r = {"id":respuesta[0]['id'],"nombre": respuesta[0]['nombre'], "apaterno": respuesta[0]['apaterno'],  "amaterno": respuesta[0]['amaterno'], "sexo": respuesta[0]['sexo'],"rfc":respuesta[0]['rfc'],"estado":respuesta[0]['estado'],"municipio":respuesta[0]['municipio'],"localidad":respuesta[0]['localidad'], "partido":respuesta[0]['partido'], "curp":respuesta[0]['curp']};
						id = respuesta[0]['id'];
						mostrar_datos(r);
						console.log(r['nombre'])
						deshabilitarcampos();
						console.log(r)
					}
				},
				error: function(respuesta){
					var mensaje = confirm("No se encuentra este registro, ¿desea registrar uno nuevo?");
					if (mensaje) {
						$("#busca_curp").val('');
						$("#busca_curp").attr("disabled",true);
						validar = true;
						habilitarcampos();
						limpiarcampos()
					}else{
						alert("Haz denegado la acción");					
					}
					
				}
			});
		}		
    });
	$(document).on("click","#exportarxls",function(e){
        // var msg = "Generando reporte...Espere por favor";
        // toastr["success"](msg);
        // Redirect("exportarxls");
        // toastr.clear();
        $.ajax({
            url:"exportarxls",
            type: 'get',
            success:function(response){
              console.log("xls");
                // toastr.clear();
                // if(response.data !=''){
                //   window.open('/reportes/excel/'+response.data,"resizeable,scrollbar");
                // }else{
                //     toastr["error"]("No existe información.");
                // }
            }
        });
    });
	// botones que actualizan o agregan un nuevo registro
	$("#agregar_materiaprima").click(function(){
		// AgregarMateriaPrima();
		$("#agregarmateriaprima_modal").modal('show');
		//regresamos el id a 0
		console.log("imprimir")
		id = 0;
		
	});
	
	//leer el cambio del select que esta en el modal para agregar mp
	$("#selecttipomateria").on('change', function(event) {
		console.log($("#selecttipomateria").val());
		$("#tipoporlitrokilo").show();
	});
	$("#pan").click(function(){
		
	});
	$("#morena").click(function(){
		AcualizaMateriaPrima();
		//regresamos el id a 0
		id = 0;
	});
	$("#otros").click(function(){
		BorrarMateriaPrima();
		//regresamos el id a 0
		id = 0;
	});
	$("#limpiar").click(function(){
		limpiarcampos();
		//regresamos el id a 0
		id = 0;
		$("#busca_curp").attr("disabled",false);
		deshabilitarcampos2();
	});

	//Esta funcion es global, permite borrar de todos los elementos
	//******=====================================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	$('.borrar').click(function (){
		console.log("entra");
		
		swal({
			title: "Estas seguro de borrar este elemento?",
			text: "Selecciona una opcion",
			icon: "warning",
			buttons: ["Cancelar", true],		
			dangerMode: true,
			})
			.then((willDelete) => {
			if (willDelete) {
				swal("Elemento borrado.", {
			 		icon: "success",
				});
			} else {
				swal("Has cancelado la accion de eliminar este elemento");
			}
		});
	});
});	
var url = "";
	function AgregarMateriaPrima(){
		var medidas = new Array();
		medidas = {
			     
			      	"valor_medida":2,
			      	"tipo_medida":"caja",
			      	"cant_pieza":1
			      	
			      }
		var otherarray = new Array();
		otherarray.push(medidas);
		otherarray.push(medidas);

		$.ajax({
			url:url+"materiaprima",
			method:"post",
			dataType: 'json',
			data: {	
				"precio":0001,
				"clave":58,
				"nombre":"Prueba borrado",
				"tipo_mat": 2,
			      "medidas":otherarray,	
				_token : $("#token").val()},
			success:function(respuesta){			
				console.log("se han agregado datos")
			},
			error: function(respuesta){
				alert("No se actualizó el registro")
			}
		});
	}
	function MostrarMateriaPrima(){
		$.ajax({
			url:url+"materiaprima",
			method:"get",
			dataType: 'json',
			data: {				
				_token : $("#token").val()},
			success:function(respuesta){			
				console.log(respuesta)
			},
			error: function(respuesta){
				alert("No se actualizó el registro")
			}
		});
	}
	function AcualizaMateriaPrima(){
		$.ajax({
			url:url+"materiaprima/2",
			method:"put",
			dataType: 'json',
			data: {
				
				nombre: "tablon",
				clave_materiaprima: "ASDF789",
				precio: 154
				,
				_token : $("#token").val()},
			success:function(respuesta){			
				
			},
			error: function(respuesta){
				
			}
		});
	}
	function BorrarMateriaPrima(){
		$.ajax({
			url:url+"materiaprima/2",
			method:"delete",
			dataType: 'json',
			data: {							
				_token : $("#token").val()},
			success:function(respuesta){			
				
			},
			error: function(respuesta){
				
			}
		});
	}
	//funcion que valida si es una actualiacion de registro o agregar registro nuevo
	function validar_llamada(validar, id, partido){
		if(validar==false){
			actualiza_datos(id, partido);
		}else{
			agregar_datos(partido);
		}
		limpiarcampos();
		deshabilitarcampos2();

	}
	var datos = new Array();
	function actualiza_datos(id, partido){
		datos.length = 0
		if(id!=""){
			datos.push({"id":id,"nombre": $("#nombre").val(), "apaterno": $("#apaterno").val(),  "amaterno": $("#amaterno").val(), "sexo": $("#sexo").val(),"rfc":$("#rfc").val(),"estado":$("#estado").val(),"municipio":$("#municipio").val(),"localidad":$("#localidad").val(), "partido":partido});
			console.log(datos)
			$.ajax({
				url:"actualizar",
				method:"post",
				dataType: 'json',
				data: {
					datos:datos,
					_token : $("#token").val()},
				success:function(respuesta){			
					alert("Registro actualizado");
					habilitarcampos();
					deshabilitarcampos();
					mostrar_datos(respuesta);
				},
				error: function(respuesta){
					alert("No se actualizó el registro")
				}
			});
		}else{
			alert("los campos estan vacios")
		}	
	}
	//funcion que deshabilita los campos de curp y rfc solo al mostrar informacion
	function deshabilitarcampos(){
		$("#curp").attr("disabled",true);
		$("#rfc").attr("disabled",true);
	}
	function deshabilitarcampos2(){
		$("#nombre").attr("disabled",true);
		$("#apaterno").attr("disabled",true);
		$("#amaterno").attr("disabled",true);
		$("#sexo").attr("disabled",true);
		$("#curp").attr("disabled",true);
		$("#rfc").attr("disabled",true);
		$("#estado").attr("disabled",true);
		$("#municipio").attr("disabled",true);
		$("#localidad").attr("disabled",true);
	}
	//funcion que habilita todos los campos, solo al momento de agregar un nuevo registro
	function habilitarcampos(){
		$("#nombre").attr("disabled",false);
		$("#apaterno").attr("disabled",false);
		$("#amaterno").attr("disabled",false);
		$("#sexo").attr("disabled",false);
		$("#curp").attr("disabled",false);
		$("#rfc").attr("disabled",false);
		$("#estado").attr("disabled",false);
		$("#municipio").attr("disabled",false);
		$("#localidad").attr("disabled",false);
	}
	function limpiarcampos(){
		$("#nombre").val('');
		$("#apaterno").val('');
		$("#amaterno").val('');
		$("#sexo").val('');
		$("#rfc").val('');
		$("#estado").val('');
		$("#municipio").val('');
		$("#localidad").val('');
		$("#curp").val('');
		$("#partido").val('');
		$("#busca_curp").val('');

	}
	function agregar_datos(partido){
		datos.length = 0;
		datos.push({"nombre": $("#nombre").val(), "apaterno": $("#apaterno").val(),  "amaterno": $("#amaterno").val(), "sexo": $("#sexo").val(),"curp":$("#curp").val(),"rfc":$("#rfc").val(),"estado":$("#estado").val(),"municipio":$("#municipio").val(),"localidad":$("#localidad").val(), "partido":partido});
		console.log(datos)
		$.ajax({
			url:"agregar",
			method:"post",
			dataType: 'json',
			data: {
				datos:datos,
				_token : $("#token").val()},
			success:function(respuesta){			
				alert("Registro agregado");
				mostrar_datos(respuesta);
				habilitarcampos()
				deshabilitarcampos()
			},
			error: function(respuesta){
				alert("No se agrego el registro")
			}
		});
	}
	//funcion de mostrar datos es la que se encarga de insertar los datos en los input
	function mostrar_datos(respuesta){
		$("#nombre").val(respuesta['nombre']);
		$("#apaterno").val(respuesta['apaterno']);
		$("#amaterno").val(respuesta['amaterno']);
		$("#sexo").val(respuesta['sexo']);
		$("#curp").val(respuesta['curp']);
		$("#rfc").val(respuesta['rfc']);
		$("#estado").val(respuesta['estado']);
		$("#municipio").val(respuesta['municipio']);
		$("#localidad").val(respuesta['localidad']);
		$("#partido").val(respuesta['partido']);
	}