<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MateriaPrima;
use App\HistorialMateriaPrima;
use App\DB;

class MateriaPrimaController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $materia = MateriaPrima::paginate(5);
    
    return view("materiaprima.materiaprima", compact('materia'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $datos = $request->all();
    $materia = MateriaPrima::create(["nombre"=>$datos["nombre"], "clave_materiaprima"=>$datos["clave"],"precio"=>$datos["precio"],"medida_id"=>1]);
    //insertamos a la tabla samedida_insumo todo lo que es extra de la materia prima a guardar
    foreach ($datos["medidas"][0] as $key => $value) {
      \DB::table("sau_medida_insumo")->insert(["id_insumo"=>$materia->id, "tipo_mat"=> 2,"valor_medida"=>$value["valor_medida"],"tipo_medida"=> $value["tipo_medida"], "cant_pieza"=>$value["cant_pieza"]]);
     
    }
    //Insertamos el valor del historico del insumo
    $historial =HistorialMateriaPrima::create(["materiaprima_id"=>$materia->id, "precio" => $materia->precio, "fecha" => date("Y-m-d h:i:s")]);
    return json_encode(["msg"=>"Elemento guardado", "materia" =>  $materia]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
   
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, Request $request)
  {
    $data = $request->all();
    $materiaprima = MateriaPrima::find($id);
    $materiaprima->nombre = $data['nombre'];
    $materiaprima->clave_materiaprima = $data['clave_materiaprima'];
    $materiaprima->precio = $data['precio'];
    $materiaprima->save();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $materiaprima = MateriaPrima::find($id);
    $materiaprima->delete();
  }
  
}

?>