<?php

namespace Pedido;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model 
{

    protected $table = 'sau_pedido';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function productos()
    {
        return $this->hasMany('PedidoProducto', 'id', 'producto_id');
    }

    public function cotizacion()
    {
        return $this->hasOne('Cotizacion', 'id');
    }

}