<?php

namespace Cotizacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotizacion extends Model 
{

    protected $table = 'sau_cotizacion';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function cliente()
    {
        return $this->hasOne('Cliente', 'id');
    }

}