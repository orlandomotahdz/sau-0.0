<?php

namespace OrdenTrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdenTrabajo extends Model 
{

    protected $table = 'sau_orden_trabajo';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function cotizacion()
    {
        return $this->hasOne('Cotizacion', 'id');
    }

}