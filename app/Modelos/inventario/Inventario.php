<?php

namespace Inventario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model 
{

    protected $table = 'sau_inventario';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}