<?php

namespace HistorialProducto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialProducto extends Model 
{

    protected $table = 'sau_historial_producto';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}