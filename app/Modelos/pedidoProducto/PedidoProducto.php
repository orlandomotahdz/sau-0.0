<?php

namespace PedidoProducto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoProducto extends Model 
{

    protected $table = 'sau_pedido_producto';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function productos()
    {
        return $this->hasOne('Producto', 'id');
    }

}