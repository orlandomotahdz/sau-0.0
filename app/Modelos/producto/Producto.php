<?php

namespace Producto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model 
{

    protected $table = 'sau_producto';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function materialutilizado()
    {
        return $this->hasMany('MaterialUtilizado', 'id', 'producto_id');
    }

    public function historialproducto()
    {
        return $this->hasOne('HistorialProducto', 'id', 'producto_id');
    }

}