<?php

namespace MaterialUtilizado;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialUtilizado extends Model 
{

    protected $table = 'sau_material_utilizado';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function materiaprima()
    {
        return $this->hasOne('MateriaPrima', 'id');
    }

}