<?php

namespace Compra;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Compra extends Model 
{

    protected $table = 'sau_compra';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function cotizacion()
    {
        return $this->hasOne('Cotizacion', 'id');
    }

}