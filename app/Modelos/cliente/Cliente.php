<?php

namespace Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model 
{

    protected $table = 'sau_cliente';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}