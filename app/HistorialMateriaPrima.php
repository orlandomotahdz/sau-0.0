<?php

namespace HistorialMateriaPrima;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialMateriaPrima extends Model 
{

    protected $table = 'sau_historial_materia_prima';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable = array(
        'materiaprima_id',
        
        'precio',
        'fecha');
    protected $dates = ['deleted_at'];

}