<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MateriaPrima extends Model 
{

    protected $table = 'sau_materia_prima';
    public $timestamps = true;

    use SoftDeletes;

    protected $fillable = array(
        'id',
        'nombre',
        'clave_materiaprima',
        'precio',
        'medida_id');
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function hisotorialmateriaprima()
    {
        return $this->hasOne('HistorialMateriaPrima', 'id');
    }

    public function inventario()
    {
        return $this->hasOne('Inventario', 'id');
    }

}