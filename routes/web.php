<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::resource('materiaprima', 'MateriaPrimaController');
Route::get('materiaprima1', 'MateriaPrimaController@store');
Route::resource('materialutilizado', 'MaterialUtilizadoController');
Route::resource('producto', 'ProductoController');
Route::resource('historialmateriaprima', 'HistorialMateriaPrimaController');
Route::resource('inventario', 'InventarioController');
Route::resource('historialproducto', 'HistorialProductoController');
Route::resource('pedido', 'PedidoController');
Route::resource('cotizacion', 'CotizacionController');
Route::resource('cliente', 'ClienteController');
Route::resource('compra', 'CompraController');
Route::resource('ordentrabajo', 'OrdenTrabajoController');
Route::resource('pedidoproducto', 'PedidoProductoController');

Auth::routes();

Route::get('/home', 'HomeController@index');
