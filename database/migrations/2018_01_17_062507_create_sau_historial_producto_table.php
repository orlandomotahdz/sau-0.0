<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauHistorialProductoTable extends Migration {

	public function up()
	{
		Schema::create('sau_historial_producto', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('producto_id');
			$table->string('precio', 50);
			$table->date('fecha');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_historial_producto');
	}
}