<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauCompraTable extends Migration {

	public function up()
	{
		Schema::create('sau_compra', function(Blueprint $table) {
			$table->increments('id');
			$table->datetime('fecha');
			$table->integer('cotizacion_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_compra');
	}
}