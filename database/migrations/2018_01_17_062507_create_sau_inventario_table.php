<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauInventarioTable extends Migration {

	public function up()
	{
		Schema::create('sau_inventario', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('materiaprima_id');
			$table->string('stock', 50);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_inventario');
	}
}