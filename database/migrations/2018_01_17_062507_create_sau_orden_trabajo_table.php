<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauOrdenTrabajoTable extends Migration {

	public function up()
	{
		Schema::create('sau_orden_trabajo', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->datetime('fecha');
			$table->integer('cotizacion_id');
		});
	}

	public function down()
	{
		Schema::drop('sau_orden_trabajo');
	}
}