<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauProductoTable extends Migration {

	public function up()
	{
		Schema::create('sau_producto', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('nombre', 100);
			$table->string('clave_producto', 50);
			$table->string('precio', 50);
			$table->integer('categoria_id');
		});
	}

	public function down()
	{
		Schema::drop('sau_producto');
	}
}