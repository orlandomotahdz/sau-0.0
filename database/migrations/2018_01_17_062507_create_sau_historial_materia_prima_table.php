<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauHistorialMateriaPrimaTable extends Migration {

	public function up()
	{
		Schema::create('sau_historial_materia_prima', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('materiaprima_id');
			$table->string('precio', 30);
			$table->date('fecha');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_historial_materia_prima');
	}
}