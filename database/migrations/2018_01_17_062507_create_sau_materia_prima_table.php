<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauMateriaPrimaTable extends Migration {

	public function up()
	{
		Schema::create('sau_materia_prima', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 191);
			$table->string('clave_materiaprima', 50);
			$table->double('precio');
			$table->integer('medida_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_materia_prima');
	}
}