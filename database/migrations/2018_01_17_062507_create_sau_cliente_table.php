<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauClienteTable extends Migration {

	public function up()
	{
		Schema::create('sau_cliente', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 20);
			$table->string('ap_paterno', 50);
			$table->string('ap_materno', 50);
			$table->string('telefono', 20);
			$table->string('empresa', 100);
			$table->string('estado', 50);
			$table->string('municipio', 50);
			$table->string('rfc', 13);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_cliente');
	}
}