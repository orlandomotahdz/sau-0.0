<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauPedidoProductoTable extends Migration {

	public function up()
	{
		Schema::create('sau_pedido_producto', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('pedido_id');
			$table->integer('producto_id');
		});
	}

	public function down()
	{
		Schema::drop('sau_pedido_producto');
	}
}