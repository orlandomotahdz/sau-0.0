<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauCotizacionTable extends Migration {

	public function up()
	{
		Schema::create('sau_cotizacion', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cliente_id');
			$table->string('clave_factura', 50);
			$table->datetime('fecha');
			$table->integer('cantidad_producto');
			$table->string('total_pagar', 50);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_cotizacion');
	}
}