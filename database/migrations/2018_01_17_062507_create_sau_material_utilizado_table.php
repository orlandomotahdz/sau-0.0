<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauMaterialUtilizadoTable extends Migration {

	public function up()
	{
		Schema::create('sau_material_utilizado', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('materiaprima_id');
			$table->integer('producto_id');
			$table->double('cantidad');
			$table->double('precio_unitario');
			$table->integer('medida_id');
			$table->string('clave', 100);
			$table->double('medida_unitaria');
			$table->double('total_piezas');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_material_utilizado');
	}
}