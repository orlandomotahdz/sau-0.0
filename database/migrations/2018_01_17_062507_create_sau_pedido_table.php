<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSauPedidoTable extends Migration {

	public function up()
	{
		Schema::create('sau_pedido', function(Blueprint $table) {
			$table->increments('id');
			$table->string('cantidad', 20);
			$table->integer('cotizacion_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sau_pedido');
	}
}